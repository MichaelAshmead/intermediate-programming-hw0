/*
Michael Ashmead, 600.120, 2/4/15, Homework 0, 484-682-9355, mashmea1, ashmeadmichael@gmail.com
*/

#include <stdio.h>
#include <string.h>

#define LENGTH 16

int main(int argc, char* argv[]) {
   char course[LENGTH];
   if (argc == 1) {
      printf("Usage: hw0 XX.###.###Gg#.#\n");
      return 1;  // exit program
   }

   strncpy(course, argv[1], LENGTH);  // copy to course string
   course[LENGTH-1] = '\0';   // make sure last character is null
   printf("course string: %s\n", course);  // echo input

   //print out course division
   //change to switch?
   if (course[0] == 'A') {
      printf("Division: %d\n", 0);
   }
   else if (course[0] == 'B') {
      printf("Division: %d\n", 1);
   }
   else if (course[0] == 'E' && course[1] == 'D') {
      printf("Division: %d\n", 2);
   }
   else if (course[0] == 'E' && course[1] == 'N') {
      printf("Division: %d\n", 3);
   }
   else if (course[0] == 'M') {
      printf("Division: %d\n", 4);
   }
   else if (course[0] == 'P' && course[1] == 'H') {
      printf("Division: %d\n", 5);
   }
   else if (course[0] == 'P' && course[1] == 'Y') {
      printf("Division: %d\n", 6);
   }
   else {
      printf("Division: %d\n", 7);
   }

   //print out course department
   printf("Department: %c%c%c\n", course[3], course[4], course[5]);

   //print out course number
   printf("Course: %c%c%c\n", course[7], course[8], course[9]);

   //print out first part of course grade
   if (course[10] == 'A') {
      printf("Grade: %d ", 0);
   }
   else if (course[10] == 'B') {
      printf("Grade: %d ", 1);
   }
   else if (course[10] == 'C') {
      printf("Grade: %d ", 2);
   }
   else if (course[10] == 'D') {
      printf("Grade: %d ", 3);
   }
   else if (course[10] == 'F') {
      printf("Grade: %d ", 4);
   }
   else if (course[10] == 'I') {
      printf("Grade: %d ", 5);
   }
   else if (course[10] == 'S') {
      printf("Grade: %d ", 6);
   }
   else {
      printf("Grade: %d ", 7);
   }

   //print out second part of course grade
   if (course[11] == '+') {
      printf("%d\n", 0);
   }
   else if (course[11] == '-') {
      printf("%d\n", 1);
   }
   else {
      printf("%d\n", 2);
   }

   //print out first part of credits
   printf("Credits: %c ", course[12]);

   //print out second part of credits
   if (course[14] == '0') {
      printf("%d\n", 0);
   }
   else {
      printf("%d\n", 1);
   }

   return 0;
}
